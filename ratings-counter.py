from pyspark import SparkConf, SparkContext
import collections

# local means only single machine no need to split into the cpu cores

conf = SparkConf().setMaster("local").setAppName("RatingsHistogram")
sc = SparkContext(conf = conf)

lines = sc.textFile("file:////Users/ozansenturk/PycharmProjects/datacamp/PySparkBigData/ml-100k/u.data")
ratings = lines.map(lambda x: x.split()[2])
result = ratings.countByValue()

sortedResults = collections.OrderedDict(sorted(result.items()))
for key, value in sortedResults.items():
    print("%s %i" % (key, value))


