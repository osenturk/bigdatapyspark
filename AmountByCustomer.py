from pyspark import SparkConf, SparkContext

conf = SparkConf().setMaster("local").setAppName("WordCount")
sc = SparkContext(conf = conf)

input = sc.textFile("data/customer-orders.csv")

def parseLine(line):
    fields = line.split(',')
    customerId = int(fields[0])
    numberOfOrders = float(fields[2])
    return (customerId, numberOfOrders)

rdd = input.map(parseLine)


totalOrdersByCustomer = rdd.reduceByKey(lambda x, y: x + y)

totalOrdersByCustomerSorted = totalOrdersByCustomer.map(lambda x: (x[1], x[0])).sortByKey()

sortedResults = totalOrdersByCustomerSorted.collect()

for result in sortedResults:
    print(result)

